# Bestandserhaltung

## ANMELDUNG

Melden sie sich in Openshift mit ihrem `oc login <command>`

## NAMESPACE ANLEGEN

Ein Namespace oder Projekt für die Anwendung anlegen. Zum Beispiel `bestandserhaltung-ns`
Zum Namespace oder Projekt wechseln.

```
oc project bestandserhaltung-ns
```


## VOLUME ANLEGEN

Ein Persistent Volume Claim für die MongoDB Datenbank anlegen. Das Volume sollte `mongodb` heißen, das kann aber durch den Wert `mongodb.persistentVolumeClaimName` geändert werden.

```
oc create pvc mongodb --storage-class=gp2 --access-modes=ReadWriteOnce --resources=requests.storage=1Gi --volume-mode=Filesystem -n bestandserhaltung-ns
```


## DOCKER

Sich mit Docker bei `docker.hub` und bei `innersource.soprasteria.com` anmelden, damit die Anmeldedaten auch in dem docker configuration file stehen.
Um sich bei `innersource.soprasteria.com` anzumelden, brauchen Sie ein Token von Sopra Steria.
Dann ein Secret anlegen mit dem Namen `bstndreg` in dem Namespace `bestandserhaltung-ns` mit dem folgenden Befehl: 

```
kubectl create secret generic bstndreg --from-file=.dockerconfigjson=$HOME/.docker/config.json --type=kubernetes.io/dockerconfigjson -n bestandserhaltung-ns
```

## INSTALL 

Der Chart zum Installation befindet sich im Verzeichnis `bestandserhaltung`.
Es müssen zwei Werte gesetzt werden, damit die Installation funktioniert: `host` und `suffix`. Host ist der Hostname der Anwendung und Suffix ist der Suffix für die Route, die wie das Namespace heißen kann. Zum Beispiel `apps.ocp4.innershift.sodigital.io` und `bestandserhaltung-ns`. Ersetzen Sie die Werte mit ensprechenden gültigen Werten, wo Host angeben sollte, wie die Route letztendlich aussehen wird.

```
cd bestandserhaltung
helm install bestandserhaltung . --set host=apps.ocp4.innershift.sodigital.io --set suffix=bestandserhaltung-ns -n bestandserhaltung-ns
```
 
oder

```
helm install bestandserhaltung bestandserhaltung-0.1.1.tgz --set host=apps.ocp4.innershift.sodigital.io --set suffix=bestandserhaltung-ns -n bestandserhaltung-ns
```

Alternativ können selbstverständlich auch die Werte in der Datei `values.yaml` angepasst werden, und auf diese Weise aufgerufen werden:

```
helm install bestandserhaltung . -f values.yaml -n bestandserhaltung-ns
helm install bestandserhaltung . -f values-openshift.yaml -n bestandserhaltung-ns
``` 




## HELM REPOSITORY

Ein Helm Repository ist unter `https://soprasteria.usercontent.opencode.de/bestandserhaltung` angelegt worden. Es enthält die Chart Versionen, die in dem Hauptverzeichnis liegen.
Eine Installation vom Helm-Repo wird so ausgeführt

```
helm repo add bestandserhaltung-repo https://soprasteria.usercontent.opencode.de/bestandserhaltung
helm repo update
helm install bestandserhaltung bestandserhaltung-repo/bestandserhaltung --set host=apps.ocp4.innershift.sodigital.io --set suffix=bestandserhaltung-ns -n bestandserhaltung-ns
```





